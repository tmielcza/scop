#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/04/11 13:30:53 by tmielcza          #+#    #+#              #
#    Updated: 2015/05/11 12:33:30 by tmielcza         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

resource_dir=resources/teapot.obj

glfw_cmake_var=GLFW_BUILD_EXAMPLES=OFF GLFW_BUILD_TESTS=OFF GLFW_BUILD_DOCS=OFF
glfw_version=glfw-3.1.1
glfw_zip_file=$(glfw_version).zip
glfw_install_dir=build
GLFW=GLFW
GLFW_CMAKE_OPTIONS=$(addprefix -D, $(glfw_cmake_var))

include_dir=$(GLFW)/$(glfw_version)/include/ include/ libft/
library_dir=$(GLFW)/$(glfw_install_dir)/src/ libft/
src_dir=src/
obj_dir=obj/
lib=glfw3 ft
framework=CoreVideo OpenGL IOKit Cocoa

CC=gcc
INCLUDE=$(addprefix -I, $(include_dir))
LIB=$(addprefix -L, $(library_dir)) $(addprefix -l, $(lib))
FRAMEWORK=$(addprefix -framework ,$(framework))

NAME=scop
CFLAGS=-Wall -Wextra -Werror $(INCLUDE)
VPATH=$(src_dir)
vpath %.c $(src_dir)

SRC=main.c \
	shader_manager.c \
	utils.c \
	vertex_buffers.c \
	view_utils.c \
	matrix_utils.c \
	shader_utils.c \
	ft_lsttoarray.c \
	parse_file.c \
	select_polys.c \
	compute_normals.c \
	bmp.c \
	del_it_dammit.c \
	input_manager.c \
	get_model_from_file.c \
	get_time.c \
	init_window.c \
	display.c \
	init_display.c \
	check_file.c

OBJ=$(SRC:%.c=$(obj_dir)%.o)

.SECONDARY: $(OBJ)

.PHONY: all clean fclean re libft

all: $(GLFW) libft $(resource_dir) $(NAME)

$(resource_dir):
	curl -OL https://projects.intrav2.42.fr/uploads/document/document/122/resources.tgz
	tar zxvf resources.tgz

libft:
	make -C libft/

$(obj_dir)%.o: %.c | $(obj_dir)
	$(CC) $(CFLAGS) -c -o $@ $<

$(obj_dir):
	mkdir -p $@

$(GLFW):
	curl -OL http://sourceforge.net/projects/glfw/files/glfw/3.1.1/$(glfw_zip_file)
	mkdir $(GLFW)
	unzip $(glfw_zip_file) -d $(GLFW)
	rm $(glfw_zip_file)
	cd $(GLFW) && \
	mkdir build && cd build && \
	cmake $(GLFW_CMAKE_OPTIONS) ../$(glfw_version) && make

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LIB) $(FRAMEWORK)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
