/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 17:24:36 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/11 12:33:15 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

# define GLFW_INCLUDE_GLCOREARB
# include <GLFW/glfw3.h>
# include "libft.h"

# define PI		3.14159265359

# define POW2(x)	(x * x)
# define TORAD(a)	(a * PI / 180.0)

typedef struct	s_vec3
{
	float		x;
	float		y;
	float		z;
}				t_vec3;

typedef struct	s_vec2
{
	float		x;
	float		y;
}				t_vec2;

typedef struct	s_shader_manager
{
	t_list		*shaders;
}				t_shader_manager;

typedef struct	s_shader
{
	GLuint		index;
	char		*name;
}				t_shader;

typedef struct	s_mat4
{
	GLfloat		mat[16];
}				t_mat4;

typedef enum	e_bool
{
	false,
	true
}				t_bool;

typedef struct	s_input_manager
{
	GLFWwindow	*win;
	t_bool		mouse_pressed;
	t_vec2		mouse_from;
	t_vec2		mouse_pos;
	double		mouse_scroll;
	t_bool		key_plus;
	t_bool		key_minus;
	t_bool		tex_switch;
	t_bool		key_0;
}				t_input_manager;

enum			e_uniform_locs
{
	_view,
	_projection,
	_texture,
	_tiling,
	_smooth,
	_uniforms_nb
};

typedef struct	s_model
{
	GLuint		vertices;
	GLuint		indices;
	size_t		indices_nb;
	GLuint		texture;
	t_vec3		position;
	t_vec3		rotation_axis;
	float		rotation_angle;
}				t_model;

char			*read_all_file(const char *name);
void			check_gl_error(const char *file, int line);

t_bool			get_shader_id(const char *file, GLenum type, GLuint *id);
GLuint			gen_buffers(const float *vertices, const float *normals,
							const float *texture_pos, size_t size);
GLuint			gen_indices_buff(const unsigned int *indices, size_t id_nb);

t_mat4			mat_mult(const t_mat4 *a, const t_mat4 *b);
void			mat_identity(t_mat4 *mat);
t_mat4			mat_translation(GLfloat x, GLfloat y, GLfloat z);
t_mat4			mat_scaling(GLfloat x, GLfloat y, GLfloat z);
t_mat4			mat_rotation(GLfloat a, GLfloat x, GLfloat y, GLfloat z);

t_mat4			mat_projection(GLfloat fov, GLfloat a, GLfloat zn, GLfloat zf);

t_bool			set_program_shaders(GLuint pgm, const char *v, const char *f);

void			*ft_lsttoarray(const t_list *lst, size_t link, size_t *total);

t_bool			parse_file(const char *file_name, t_list **vrtx, t_list **ids,
							float (*center)[3]);

void			select_polys(t_list **polys);

float			*compute_normals(const float *vertices,
								const unsigned int *triangles,
								size_t vertices_nb, size_t tris_nb);

GLuint			texture_from_bmp(char *name);

void			del_it_dammit(void *content, size_t s);

t_input_manager	*get_input_manager_instance(void);
void			update_mouse_wheel(GLFWwindow *win, double x, double y);
void			update_cursor_position(GLFWwindow *win, double x, double y);
void			update_mouse_click(GLFWwindow *win, int button, int action,
									int mods);
void			update_keys(void);

t_bool			get_model_from_file(const char *filename, t_model *model);

float			get_time(void);

GLFWwindow		*create_glfw_window(int w, int h);

t_bool			display_loop(GLFWwindow *win, const char *filename);

t_bool			init_display(t_vec3 *cam, float *tiling, t_model *model,
							float *smooth);

t_bool			check_model_data(const t_list *vertices, const t_list *indices);

#endif
