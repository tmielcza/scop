/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/12 19:23:34 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/09 15:43:50 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void		ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*tmp;
	t_list	*tofree;

	tmp = *alst;
	while (tmp != NULL)
	{
		del(tmp->content, tmp->content_size);
		tofree = tmp;
		tmp = tmp->next;
		free(tofree);
	}
	*alst = NULL;
}
