/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/12 20:01:05 by tmielcza          #+#    #+#             */
/*   Updated: 2015/04/12 20:11:35 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*tmp;

	tmp = NULL;
	while (lst != NULL)
	{
		if (tmp == NULL)
		{
			if (!(tmp = f(lst)))
			{
				return (NULL);
			}
			new = tmp;
		}
		if (!(tmp->next = f(lst)))
		{
			return (NULL);
		}
		lst = lst->next;
	}
	return (new);
}
