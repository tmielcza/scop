/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 13:13:33 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/07 17:55:24 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

void		*ft_memalloc(size_t size)
{
	void	*ret;

	ret = (char *)malloc(size * sizeof(char));
	if (ret)
		ft_bzero(ret, size);
	return (ret);
}
