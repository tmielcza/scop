/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 17:35:15 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:00:06 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void		*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char		*tmpd;
	unsigned const char	*tmps;
	size_t				i;

	i = 0;
	tmpd = dest;
	tmps = src;
	while (i < n)
	{
		if ((tmpd[i] = tmps[i]) == (unsigned char)c)
		{
			return (tmpd + i + 1);
		}
		i++;
	}
	return (NULL);
}
