/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:51:32 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/07 20:27:53 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <libft.h>

void		*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	cc;
	unsigned char	*cha;

	if (!s)
		return (NULL);
	cc = c;
	cha = (unsigned char *)s;
	while (n > 0)
	{
		if (*(cha) == cc)
			return (cha);
		n--;
		cha++;
	}
	return (NULL);
}
