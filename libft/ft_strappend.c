/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strappend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/12 19:38:36 by tmielcza          #+#    #+#             */
/*   Updated: 2015/04/12 19:43:10 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strappend(char **s1, char *s2)
{
	char		*s;

	if (*s1 && s2)
	{
		if (!(s = ft_strnew(ft_strlen(*s1) + ft_strlen(s2) + 1)))
			return (NULL);
		ft_strcpy(s, *s1);
		ft_strcat(s, s2);
		*s1 = s;
		return (*s1);
	}
	return (NULL);
}
