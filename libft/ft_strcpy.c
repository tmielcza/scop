/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 18:16:29 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/07 17:54:27 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char		*ft_strcpy(char *dest, const char *src)
{
	ft_memmove(dest, src, ft_strlen(src) + 1);
	return (dest);
}
