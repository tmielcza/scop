/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:16:44 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/08 12:36:37 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	ldst;
	size_t	lsrc;
	int		ltotal;

	lsrc = ft_strlen(src);
	ldst = ft_strlen(dst);
	ltotal = size - ldst - 1;
	ltotal = ltotal >= 0 ? ltotal : 0;
	if (size < ldst)
		return (size + lsrc);
	ft_strncpy(dst + ldst, src, ltotal);
	dst[size - 1] = '\0';
	return (ldst + lsrc);
}
