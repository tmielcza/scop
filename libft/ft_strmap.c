/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 14:27:31 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/10 20:19:19 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	size_t	n;

	n = ft_strlen(s);
	str = ft_strnew(n);
	str[n] = '\0';
	while (n != 0)
	{
		n--;
		str[n] = f(s[n]);
	}
	return (str);
}
