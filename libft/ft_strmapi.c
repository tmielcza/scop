/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 14:38:20 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/10 20:19:15 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*str;
	size_t	n;

	n = ft_strlen(s);
	str = ft_strnew(n);
	str[n] = '\0';
	while (n != 0)
	{
		n--;
		str[n] = f(s[n], n);
	}
	return (str);
}
