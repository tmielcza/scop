/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:32:41 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/07 17:53:34 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char		*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t		j;
	size_t		i;

	if (!*s2)
		return ((char *)s1);
	i = 0;
	j = 0;
	while (s1[i] != 0 && i < n)
	{
		while (s1[i + j] && s1[i + j] == s2[j] && i + j < n)
		{
			j++;
		}
		if (s2[j] == 0)
			return ((char *)s1 + i);
		j = 0;
		i++;
	}
	return (NULL);
}
