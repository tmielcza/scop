/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:27:55 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/07 17:53:31 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char		*ft_strrchr(const char *s, int c)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)s + ft_strlen((char *)s);
	while (str[i] != (char)c && (str + i) != s)
		i--;
	if (str[i] == (char)c)
		return ((char *)(str + i));
	return (NULL);
}
