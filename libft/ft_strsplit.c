/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 15:05:55 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/10 21:33:23 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		**ft_strsplit(char const *s, char c)
{
	char	**ret;
	int		i;
	int		j;
	int		words;

	i = 0;
	j = 0;
	words = 0;
	ret = (char **)malloc(ft_strlen(s) * sizeof(char *) / 2);
	while (ret != NULL && s[i] != '\0')
	{
		if (s[i] == c)
		{
			if (j != 0 && (ret[words++] = ft_strsub(s, i - j, j)))
				j = 0;
		}
		else
			j++;
		i++;
	}
	if (j != 0)
		ret[words++] = ft_strsub(s, i - j, j);
	ret[words] = NULL;
	return (ret);
}
