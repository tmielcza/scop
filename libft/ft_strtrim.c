/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 15:03:49 by tmielcza          #+#    #+#             */
/*   Updated: 2015/03/11 15:32:23 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*ft_strtrim(char const *s)
{
	char	*ret;
	int		st;
	int		ed;

	st = 0;
	ed = ft_strlen(s) - 1;
	while (s[st] != '\0' && (s[st] == ' ' || s[st] == '\t' || s[st] == '\n'))
	{
		st++;
	}
	while (ed >= st && (s[ed] == ' ' || s[ed] == '\t' || s[ed] == '\n'))
	{
		ed--;
	}
	ret = ft_strsub(s, st, ed - st + 1);
	if (ret != NULL)
	{
		return (ret);
	}
	return (NULL);
}
