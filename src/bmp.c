/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bmp.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 16:03:48 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/08 19:08:28 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "scop.h"

static GLuint	new_tex(int w, int h, char *data)
{
	GLuint	id;

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h,
				0, GL_BGR, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
					GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
					GL_NEAREST_MIPMAP_NEAREST);
	return (id);
}

static void		*load_bmp_tex(char *name, int *offset, int *w, int *h)
{
	FILE			*file;
	char			*data;
	unsigned int	size;

	file = fopen(name, "r");
	if (file == NULL)
	{
		ft_putstr_fd("Can't open file for reasons.\n", 2);
		return (0);
	}
	if ((data = (char *)malloc(54 * sizeof(char))) == NULL)
		return (NULL);
	if (!fread(data, sizeof(char), 54, file))
		return (NULL);
	*w = *(int *)(data + 0x12);
	*h = *(int *)(data + 0x16);
	*offset = *(int *)(data + 0x0A);
	size = *w * *h * 3;
	free(data);
	if ((data = (char *)malloc((size) * sizeof(char))) == NULL)
		return (NULL);
	if (!fread(data, sizeof(char), size, file))
		return (NULL);
	fclose(file);
	return (data);
}

GLuint			texture_from_bmp(char *name)
{
	GLuint		tex;
	void		*data;
	int			offset;
	int			width;
	int			height;

	data = load_bmp_tex(name, &offset, &width, &height);
	if (data == NULL)
		return (0);
	tex = new_tex(width, height, data);
	free(data);
	return (tex);
}
