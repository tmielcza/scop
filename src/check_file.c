/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/11 12:23:31 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/11 12:40:30 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_bool		check_model_data(const t_list *vertices, const t_list *indices)
{
	size_t			count;
	unsigned int	*ids;

	count = 0;
	while (vertices != NULL)
	{
		vertices = vertices->next;
		count++;
	}
	while (indices != NULL)
	{
		ids = (unsigned int *)indices->content;
		if (ids[0] > count || ids[1] > count || ids[2] > count)
			return (false);
		indices = indices->next;
	}
	return (true);
}
