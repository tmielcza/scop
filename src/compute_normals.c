/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compute_normals.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/30 14:51:38 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/09 15:43:44 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include "scop.h"

static void		get_normal(float *norm, const float *a,
							const float *b, const float *c)
{
	float	u[3];
	float	v[3];

	u[0] = a[0] - b[0];
	u[1] = a[1] - b[1];
	u[2] = a[2] - b[2];
	v[0] = c[0] - b[0];
	v[1] = c[1] - b[1];
	v[2] = c[2] - b[2];
	norm[0] = u[1] * v[2] - u[2] * v[1];
	norm[1] = u[2] * v[0] - u[0] * v[2];
	norm[2] = u[0] * v[1] - u[1] * v[0];
}

static void		normalize(float *vec)
{
	float		mag;

	mag = sqrt(POW2(vec[0]) + POW2(vec[1]) + POW2(vec[2]));
	vec[0] /= mag;
	vec[1] /= mag;
	vec[2] /= mag;
}

static void		compute_normal(float (*normal)[3], const t_list *faces,
								const float *vertices)
{
	unsigned int	(*tmp_face)[3];
	float			norm[3];

	while (faces != NULL)
	{
		tmp_face = (unsigned int (*)[3])(faces->content);
		get_normal(norm, &vertices[3 * (*tmp_face)[0]],
					&vertices[3 * (*tmp_face)[1]],
					&vertices[3 * (*tmp_face)[2]]);
		(*normal)[0] += norm[0];
		(*normal)[1] += norm[1];
		(*normal)[2] += norm[2];
		faces = faces->next;
	}
	normalize((float *)normal);
}

float			*compute_normals(const float *vertices,
								const unsigned int *triangles,
								size_t vertices_nb, size_t tris_nb)
{
	t_list			**faces_with_i;
	unsigned int	i;
	t_list			*tmp;
	float			*normals;

	i = 0;
	normals = (float *)malloc(vertices_nb * sizeof(float) * 3);
	faces_with_i = (t_list **)malloc(vertices_nb * sizeof(t_list *));
	ft_bzero(faces_with_i, vertices_nb * sizeof(t_list *));
	ft_bzero(normals, vertices_nb * sizeof(float) * 3);
	while (i < tris_nb * 3)
	{
		tmp = ft_lstnew(triangles + i - i % 3, 3 * sizeof(triangles[0]));
		ft_lstadd(&faces_with_i[triangles[i]], tmp);
		i++;
	}
	i = 0;
	while (i < vertices_nb)
	{
		compute_normal((void *)&normals[i * 3], faces_with_i[i], vertices);
		ft_lstdel(&faces_with_i[i], del_it_dammit);
		i++;
	}
	free(faces_with_i);
	return (normals);
}
