/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 18:08:41 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/11 12:42:31 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		display_test(GLFWwindow *win, const t_model *model)
{
	glClearColor(1.0f, 5.0f, 3.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->indices);
	glDrawElements(GL_TRIANGLES, model->indices_nb, GL_UNSIGNED_INT, NULL);
	glfwSwapBuffers(win);
}

t_bool			display_loop(GLFWwindow *win, const char *filename)
{
	t_vec3		cam;
	float		tiling;
	t_model		model;
	float		smoothness;

	smoothness = 0;
	tiling = 10;
	ft_bzero(&model, sizeof(model));
	cam.x = 0;
	cam.y = 0;
	cam.z = -200;
	if (!get_model_from_file(filename, &model))
	{
		ft_putstr_fd("Il est nul ton fichier.\n", 2);
		return (false);
	}
	while (!glfwWindowShouldClose(win))
	{
		if (!init_display(&cam, &tiling, &model, &smoothness))
			return (true);
		display_test(win, &model);
		glfwPollEvents();
	}
	return (true);
}
