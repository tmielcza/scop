/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsttoarray.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 13:07:38 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/09 15:06:49 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_lsttoarray(const t_list *lst, size_t link, size_t *total)
{
	void			*ret;
	const t_list	*tmp;
	char			*cptmp;

	if (*total == 0)
	{
		tmp = lst;
		while (tmp)
		{
			*total += link;
			tmp = tmp->next;
		}
		if (*total == 0)
			return (NULL);
	}
	ret = (void *)malloc(*total);
	cptmp = ret;
	while (lst != NULL)
	{
		ft_memcpy(cptmp, lst->content, link);
		cptmp += link;
		lst = lst->next;
	}
	return (ret);
}
