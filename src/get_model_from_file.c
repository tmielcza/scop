/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_model_from_file.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 22:16:55 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/11 12:40:11 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "scop.h"

static t_bool	geometry_to_gpu(const t_list *vertices, const t_list *indices,
								const t_list *normals, t_model *model)
{
	void	*faces_buff;
	void	*vert_buff;
	void	*norm_buff;
	size_t	vert_size;
	size_t	faces_size;

	faces_buff = NULL;
	vert_buff = NULL;
	faces_buff = NULL;
	vert_size = 0;
	faces_size = 0;
	vert_buff = ft_lsttoarray(vertices, vertices->content_size, &vert_size);
	faces_buff = ft_lsttoarray(indices, indices->content_size, &faces_size);
	model->indices_nb = faces_size;
	if (normals == NULL)
		norm_buff = compute_normals(vert_buff, faces_buff, vert_size /
									(sizeof(t_vec3)), faces_size / (3 * 4));
	else
		norm_buff = ft_lsttoarray(normals, normals->content_size, NULL);
	model->vertices = gen_buffers(vert_buff, norm_buff, NULL, vert_size);
	model->indices = gen_indices_buff(faces_buff, model->indices_nb);
	free(norm_buff);
	free(vert_buff);
	free(faces_buff);
	return (true);
}

t_bool			get_model_from_file(const char *filename, t_model *model)
{
	t_list	*v;
	t_list	*i;

	v = NULL;
	i = NULL;
	if (!parse_file(filename, &v, &i, (float (*)[3])&model->position))
		return (false);
	if (!v || !i)
		return (false);
	select_polys(&i);
	if (!check_model_data(v, i))
		return (false);
	geometry_to_gpu(v, i, NULL, model);
	ft_lstdel(&v, del_it_dammit);
	ft_lstdel(&i, del_it_dammit);
	return (true);
}
