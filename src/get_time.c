/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_time.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 15:48:22 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/10 15:48:45 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sys/time.h"

float			get_time(void)
{
	static struct timeval	t1 = {0, 0};
	struct timeval			t2;
	float					time;

	if (t1.tv_usec == 0)
	{
		gettimeofday(&t1, NULL);
	}
	gettimeofday(&t2, NULL);
	time = (t2.tv_sec - t1.tv_sec) * 1000.0;
	time += (t2.tv_usec - t1.tv_usec) / 1000.0;
	return (time / 1000.f);
}
