/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_display.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 18:13:10 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/10 19:25:42 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

static void		init_display_view(const GLuint uniforms[_uniforms_nb],
								const t_vec3 *cam, const t_vec3 *center,
								float time)
{
	t_mat4	proj;
	t_mat4	view;
	t_mat4	mat;

	mat_identity(&view);
	mat = mat_translation(cam->x, cam->y, cam->z);
	view = mat_mult(&view, &mat);
	mat = mat_rotation(time * PI / 2, 0.f, 1.f, 0.f);
	view = mat_mult(&view, &mat);
	mat = mat_translation(-center->x, -center->y, -center->z);
	view = mat_mult(&view, &mat);
	proj = mat_projection(30.0f, 640.0f / 480.0f, 1.0f, 100.0f);
	glUniformMatrix4fv(uniforms[_view], 1, GL_TRUE, view.mat);
	glUniformMatrix4fv(uniforms[_projection], 1, GL_TRUE, proj.mat);
}

static void		update_camera(t_vec3 *cam)
{
	static t_bool	position_updated = false;
	static t_vec3	old_cam = {0, 0, 0};
	t_input_manager	*im;
	const float		s = 500;

	im = get_input_manager_instance();
	if (im->mouse_scroll != 0)
	{
		cam->z *= 1 + im->mouse_scroll / 1000;
		im->mouse_scroll = 0;
	}
	if (im->mouse_pressed)
	{
		if (position_updated)
			position_updated = false;
		cam->x = old_cam.x + (im->mouse_pos.x - im->mouse_from.x) * cam->z / s;
		cam->y = old_cam.y + (im->mouse_from.y - im->mouse_pos.y) * cam->z / s;
	}
	else if (!position_updated)
	{
		old_cam.x += (im->mouse_pos.x - im->mouse_from.x) * cam->z / s;
		old_cam.y += (im->mouse_from.y - im->mouse_pos.y) * cam->z / s;
		old_cam.z = cam->z;
		position_updated = true;
	}
}

static void		update_tiling(float *tiling)
{
	update_keys();
	if (get_input_manager_instance()->key_plus)
	{
		*tiling *= 1.02;
	}
	else if (get_input_manager_instance()->key_minus)
	{
		*tiling /= 1.02;
	}
}

static void		init_texture(GLuint prog)
{
	GLuint		loc;
	GLuint		tex_id;

	tex_id = texture_from_bmp("resources/cat.bmp");
	glActiveTexture(GL_TEXTURE0);
	loc = glGetUniformLocation(prog, "tex");
	glUniform1i(loc, 0);
}

t_bool			init_display(t_vec3 *cam, float *tiling, t_model *model,
							float *smooth)
{
	GLuint		uniforms[_uniforms_nb];
	GLuint		program;

	program = glCreateProgram();
	if (!set_program_shaders(program, "test.vert", "test.frag"))
		return (false);
	glBindAttribLocation(program, 0, "position");
	glBindAttribLocation(program, 1, "normal");
	uniforms[_view] = glGetUniformLocation(program, "viewMatrix");
	uniforms[_projection] = glGetUniformLocation(program, "projMatrix");
	uniforms[_texture] = glGetUniformLocation(program, "tex");
	uniforms[_tiling] = glGetUniformLocation(program, "tiling");
	uniforms[_smooth] = glGetUniformLocation(program, "Smooth");
	init_texture(program);
	update_camera(cam);
	update_tiling(tiling);
	glUniform1f(uniforms[_tiling], *tiling);
	if (get_input_manager_instance()->tex_switch && *smooth < 1.0)
		*smooth += 0.02;
	else if (!get_input_manager_instance()->tex_switch && *smooth > 0.0)
		*smooth -= 0.02;
	glUniform1f(uniforms[_smooth], *smooth);
	init_display_view(uniforms, cam, &model->position, get_time());
	return (true);
}
