/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_window.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/10 16:07:14 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/10 17:49:40 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

GLFWwindow		*create_glfw_window(int w, int h)
{
	GLFWwindow	*win;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	win = glfwCreateWindow(w, h, "SCOP", NULL, NULL);
	glfwMakeContextCurrent(win);
	glfwSetCursorPosCallback(win, update_cursor_position);
	glfwSetMouseButtonCallback(win, update_mouse_click);
	glfwSetScrollCallback(win, update_mouse_wheel);
	get_input_manager_instance()->win = win;
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	return (win);
}
