/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_manager.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/08 16:33:50 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/10 19:24:33 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "scop.h"

t_input_manager	*get_input_manager_instance(void)
{
	static t_input_manager	instance;

	return (&instance);
}

void			update_keys(void)
{
	t_input_manager	*im;

	im = get_input_manager_instance();
	im->key_plus = glfwGetKey(im->win, GLFW_KEY_KP_ADD) == GLFW_PRESS;
	im->key_minus = glfwGetKey(im->win, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS;
	if (glfwGetKey(im->win, GLFW_KEY_1) == GLFW_PRESS)
	{
		if (im->key_0 == false)
		{
			im->tex_switch = im->tex_switch != true;
			im->key_0 = true;
		}
	}
	else if (im->key_0 != false)
	{
		im->key_0 = false;
	}
}

void			update_mouse_wheel(GLFWwindow *win, double x, double y)
{
	t_input_manager	*im;

	im = get_input_manager_instance();
	(void)win;
	(void)x;
	im->mouse_scroll += y;
}

void			update_cursor_position(GLFWwindow *win, double x, double y)
{
	t_input_manager	*im;

	im = get_input_manager_instance();
	(void)win;
	im->mouse_pos.x = x;
	im->mouse_pos.y = y;
}

void			update_mouse_click(GLFWwindow *win, int button, int action,
									int mods)
{
	t_input_manager	*im;

	im = get_input_manager_instance();
	(void)win;
	(void)mods;
	if (button == GLFW_MOUSE_BUTTON_1)
	{
		if (action == GLFW_PRESS && im->mouse_pressed == false)
		{
			im->mouse_pressed = true;
			im->mouse_from.x = im->mouse_pos.x;
			im->mouse_from.y = im->mouse_pos.y;
		}
		else if (action == GLFW_RELEASE && im->mouse_pressed == true)
			im->mouse_pressed = false;
	}
}
