/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 16:20:36 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/10 18:20:47 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include "scop.h"

int				main(int ac, char **av)
{
	GLFWwindow	*win;

	if (ac != 2)
	{
		ft_putendl("Usage: ./scop <file.obj>\n");
		return (1);
	}
	if (!glfwInit())
	{
		return (1);
	}
	win = create_glfw_window(640, 480);
	display_loop(win, av[1]);
	glfwDestroyWindow(win);
	glfwTerminate();
	return (0);
}
