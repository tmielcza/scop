/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/28 14:08:01 by tmielcza          #+#    #+#             */
/*   Updated: 2015/04/28 18:32:55 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "scop.h"

t_mat4	mat_mult(const t_mat4 *a, const t_mat4 *b)
{
	t_mat4	c;
	int		i;

	i = 0;
	while (i < 16)
	{
		c.mat[i] = a->mat[i / 4 * 4 + 0] * b->mat[i % 4 + 0];
		c.mat[i] += a->mat[i / 4 * 4 + 1] * b->mat[i % 4 + 4];
		c.mat[i] += a->mat[i / 4 * 4 + 2] * b->mat[i % 4 + 8];
		c.mat[i] += a->mat[i / 4 * 4 + 3] * b->mat[i % 4 + 12];
		i++;
	}
	return (c);
}

void	mat_identity(t_mat4 *mat)
{
	ft_memset(mat, 0, 16 * sizeof(float));
	mat->mat[0] = 1;
	mat->mat[5] = 1;
	mat->mat[10] = 1;
	mat->mat[15] = 1;
}

t_mat4	mat_translation(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;

	mat_identity(&mat);
	mat.mat[3] = x;
	mat.mat[7] = y;
	mat.mat[11] = z;
	return (mat);
}

t_mat4	mat_scaling(GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;

	mat_identity(&mat);
	mat.mat[0] = x;
	mat.mat[5] = y;
	mat.mat[10] = z;
	return (mat);
}

t_mat4	mat_rotation(GLfloat a, GLfloat x, GLfloat y, GLfloat z)
{
	t_mat4	mat;
	GLfloat	c;
	GLfloat	s;

	c = cos(a);
	s = sin(a);
	mat_identity(&mat);
	mat.mat[0] = POW2(x) + (1 - POW2(x)) * c;
	mat.mat[1] = x * y * (1 - c) - z * s;
	mat.mat[2] = x * z * (1 - c) + y * s;
	mat.mat[4] = x * y * (1 - c) + z * s;
	mat.mat[5] = POW2(y) + (1 - POW2(y)) * c;
	mat.mat[6] = y * z * (1 - c) - x * s;
	mat.mat[8] = x * z * (1 - c) - y * s;
	mat.mat[9] = y * z * (1 - c) + x * s;
	mat.mat[10] = POW2(z) + (1 - POW2(z)) * c;
	return (mat);
}
