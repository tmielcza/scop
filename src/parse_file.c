/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 13:59:03 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/11 12:28:22 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "scop.h"

static void	parse_line(const char *line, t_list ***vertices_end,
						t_list ***indices_end)
{
	float			vec[3];
	unsigned int	f[3][4];
	int				test;

	if ((test = sscanf(line, "v %f %f %f", &vec[0], &vec[1], &vec[2])) > 0)
	{
		ft_lstadd(*vertices_end, ft_lstnew(vec, sizeof(vec)));
		*vertices_end = &(**vertices_end)->next;
	}
	if (((test = sscanf(line, "f %u %u %u %u", &f[0][0], &f[0][1], &f[0][2],
						&f[0][3])) > 2 && test < 5)
		|| ((test = sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u",
							&f[0][0], &f[1][0], &f[2][0],
							&f[0][1], &f[1][1], &f[2][1],
							&f[0][2], &f[1][2], &f[2][2]) / 3) == 3)
		|| ((test = sscanf(line, "f %u/" "/%u %u/" "/%u %u/" "/%u", &f[0][0],
				&f[2][0], &f[0][1], &f[2][1], &f[0][2], &f[2][2]) / 2) == 3))
	{
		f[0][0]--;
		f[0][1]--;
		f[0][2]--;
		f[0][3]--;
		ft_lstadd(*indices_end, ft_lstnew(f, test * sizeof(unsigned int)));
		*indices_end = &(**indices_end)->next;
	}
}

static void	get_center(float (*center)[3], const t_list *vertices)
{
	int		count;

	count = 0;
	while (vertices != NULL)
	{
		(*center)[0] += ((float *)vertices->content)[0];
		(*center)[1] += ((float *)vertices->content)[1];
		(*center)[2] += ((float *)vertices->content)[2];
		vertices = vertices->next;
		count++;
	}
	(*center)[0] /= count;
	(*center)[1] /= count;
	(*center)[2] /= count;
}

t_bool		parse_file(const char *file_name, t_list **vertices,
						t_list **indices, float (*center)[3])
{
	FILE	*f;
	t_list	**vtxend;
	t_list	**idxend;
	size_t	size;
	char	*bite;

	size = 0;
	bite = NULL;
	vtxend = vertices;
	idxend = indices;
	f = fopen(file_name, "r");
	if (f == NULL)
	{
		ft_putstr_fd("Error when openning file: ", 2);
		ft_putstr_fd(strerror(errno), 2);
		return (false);
	}
	while (getline(&bite, &size, f) > 0)
	{
		parse_line(bite, &vtxend, &idxend);
	}
	free(bite);
	get_center(center, *vertices);
	return (true);
}
