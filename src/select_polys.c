/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select_polys.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 18:32:08 by tmielcza          #+#    #+#             */
/*   Updated: 2015/04/30 14:03:41 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "scop.h"

#include <stdio.h>

void	select_polys(t_list **polys)
{
	t_list			*quad;
	unsigned int	face[3];
	t_list			*tmp;

	while (*polys != NULL)
	{
		if ((*polys)->content_size == 4 * sizeof(unsigned int))
		{
			face[0] = ((unsigned int *)((*polys)->content))[0];
			face[1] = ((unsigned int *)((*polys)->content))[1];
			face[2] = ((unsigned int *)((*polys)->content))[2];
			quad = ft_lstnew(face, sizeof(face));
			face[0] = ((unsigned int *)((*polys)->content))[2];
			face[1] = ((unsigned int *)((*polys)->content))[3];
			face[2] = ((unsigned int *)((*polys)->content))[0];
			ft_lstadd(&quad->next, ft_lstnew(face, sizeof(face)));
			free((*polys)->content);
			tmp = (*polys)->next;
			free(*polys);
			*polys = quad;
			(*polys)->next->next = tmp;
		}
		polys = &(*polys)->next;
	}
}
