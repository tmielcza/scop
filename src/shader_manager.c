/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader_manager.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 17:39:48 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/08 16:10:14 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <GLFW/glfw3.h>
#include "scop.h"

static t_shader_manager	*get_instance(void)
{
	static t_shader_manager	sm;

	return (&sm);
}

static void				print_shader_error(GLuint shader)
{
	GLint				max_length;
	char				*log;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_length);
	log = (char *)malloc(max_length);
	glGetShaderInfoLog(shader, max_length, &max_length, log);
	ft_putstr_fd(log, 2);
	glDeleteShader(shader);
	free(log);
}

static t_shader			*new_shader(const char *file, GLenum type)
{
	t_shader			*shader;
	GLuint				id;
	GLint				is_compiled;
	char				*data;

	if (!(data = read_all_file(file)))
	{
		return (NULL);
	}
	id = glCreateShader(type);
	glShaderSource(id, 1, (const char **)&data, NULL);
	glCompileShader(id);
	glGetShaderiv(id, GL_COMPILE_STATUS, &is_compiled);
	free(data);
	if (is_compiled == GL_FALSE)
	{
		print_shader_error(id);
		return (NULL);
	}
	shader = (t_shader *)malloc(sizeof(t_shader));
	shader->name = ft_strdup(file);
	shader->index = id;
	return (shader);
}

t_bool					get_shader_id(const char *file, GLenum type, GLuint *id)
{
	t_list		*tmp;
	t_shader	*shad;

	tmp = get_instance()->shaders;
	while (tmp != NULL)
	{
		if (ft_strequ(file, ((t_shader *)tmp->content)->name))
		{
			*id = ((t_shader *)tmp->content)->index;
			return (true);
		}
		tmp = tmp->next;
	}
	if ((shad = new_shader(file, type)))
	{
		*id = shad->index;
		ft_lstadd(&get_instance()->shaders, ft_lstnew(shad, sizeof(*shad)));
		free(shad);
		return (true);
	}
	return (false);
}
