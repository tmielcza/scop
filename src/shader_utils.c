/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shader_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/28 18:52:15 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/08 17:39:07 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "scop.h"

static void	print_error_infos(GLuint program)
{
	GLint	max_length;
	char	*info_log;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &max_length);
	if ((info_log = (char *)malloc(max_length)) == NULL)
	{
		return ;
	}
	glGetProgramInfoLog(program, max_length, &max_length, info_log);
	ft_putendl("Cannot link program:");
	ft_putendl(info_log);
	free(info_log);
	glDeleteProgram(program);
}

t_bool		set_program_shaders(GLuint prog, const char *vert, const char *frag)
{
	GLuint	vert_id;
	GLuint	frag_id;
	GLint	is_linked;

	if (!get_shader_id(vert, GL_VERTEX_SHADER, &vert_id) ||
		!get_shader_id(frag, GL_FRAGMENT_SHADER, &frag_id))
		return (false);
	glAttachShader(prog, vert_id);
	glAttachShader(prog, frag_id);
	glLinkProgram(prog);
	glGetProgramiv(prog, GL_LINK_STATUS, &is_linked);
	if (is_linked == GL_FALSE)
	{
		print_error_infos(prog);
		return (false);
	}
	glUseProgram(prog);
	return (true);
}
