/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 20:32:40 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/07 16:15:31 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include "libft.h"
#include "scop.h"

void		check_gl_error(const char *file, int line)
{
	GLuint	err;

	err = glGetError();
	if (err != GL_FALSE)
	{
		ft_putstr("ERROR in ");
		ft_putstr(file);
		ft_putstr(" l.");
		ft_putnbr(line);
		ft_putstr(": ");
		ft_putnbr(err);
		ft_putendl(".");
	}
}

char		*read_all_file(const char *name)
{
	FILE	*file;
	size_t	size;
	char	*data;

	file = fopen(name, "r");
	if (file == NULL)
	{
		ft_putstr_fd("Error:", 2);
		ft_putstr_fd(strerror(errno), 2);
		return (NULL);
	}
	fseek(file, 0L, SEEK_END);
	size = ftell(file);
	fseek(file, 0L, SEEK_SET);
	data = malloc(size + 1);
	fread(data, size, 1, file);
	fclose(file);
	data[size] = 0;
	return (data);
}
