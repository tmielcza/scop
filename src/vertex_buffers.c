/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex_buffers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/27 14:57:51 by tmielcza          #+#    #+#             */
/*   Updated: 2015/05/09 14:55:51 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "scop.h"

static void	setup_attrib_vbo(const float *values, size_t size, GLuint buff_id,
					GLuint attrib_id)
{
	glBindBuffer(GL_ARRAY_BUFFER, buff_id);
	glBufferData(GL_ARRAY_BUFFER, size, values, GL_STATIC_DRAW);
	glVertexAttribPointer(attrib_id, 3, GL_FLOAT, 0, 0, 0);
	glEnableVertexAttribArray(attrib_id);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLuint		gen_buffers(const float *vertices, const float *normals,
						const float *texture_pos, size_t size)
{
	GLuint	buffers[3];
	GLuint	vao;
	int		buffers_nb;

	buffers_nb = !(vertices == NULL) + !(normals == NULL)
		+ !(texture_pos == NULL);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(buffers_nb, buffers);
	setup_attrib_vbo(vertices, size, buffers[0], 0);
	if (normals != NULL)
	{
		setup_attrib_vbo(normals, size, buffers[1], 1);
	}
	if (texture_pos != NULL)
	{
		setup_attrib_vbo(normals, size, buffers[buffers_nb - 1],
						buffers_nb - 1);
	}
	return (vao);
}

GLuint		gen_indices_buff(const unsigned int *indices, size_t indices_nb)
{
	GLuint	ibo;

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_nb, indices, GL_STATIC_DRAW);
	return (ibo);
}
