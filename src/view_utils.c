/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   view_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/28 14:24:12 by tmielcza          #+#    #+#             */
/*   Updated: 2015/04/28 14:57:45 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <math.h>
#include "scop.h"

t_mat4	mat_projection(GLfloat fov, GLfloat asp, GLfloat znear, GLfloat zfar)
{
	t_mat4	m;
	GLfloat	xmax;
	GLfloat	ymax;

	ft_bzero(m.mat, 16 * sizeof(float));
	ymax = znear * (float)tan(TORAD(fov));
	xmax = ymax * asp;
	m.mat[0] = (2.0f * znear) / (2.0f * xmax);
	m.mat[5] = (2.0f * znear) / (2.0f * ymax);
	m.mat[10] = (-zfar - znear) / (zfar - znear);
	m.mat[11] = -1.0f;
	m.mat[14] = (-(2.0f * znear) * zfar) / (zfar - znear);
	return (m);
}
