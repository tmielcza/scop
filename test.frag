#version 400

uniform mat4 viewMatrix, projMatrix;
uniform sampler2D tex;
uniform float tiling;
uniform float Smooth;

in vec3 Position;
in vec4 Color;
in vec3 Normal;
in vec3 LightVec;
in vec3 rNormal;
flat in vec4 shit;

out vec4 fragColor;

void main()
{
	vec2 uv;
	vec4 text;
	if (abs(Normal.x) < 0.576 && abs(Normal.y) < 0.576)
		uv = vec2(Position.x, Position.y);
	else if (abs(Normal.x) < 0.576 && abs(Normal.z) < 0.576)
		uv = vec2(Position.x, Position.z);
	else
		uv = vec2(Position.z, Position.y);
	uv /= tiling;
	text = texture(tex, uv);
	vec4 amb = vec4(0.2, 0.2, 0.2, 1.0);
	vec3 reflectRay = reflect(LightVec, rNormal);
	vec4 spec = vec4(vec3(1, 1, 1) * pow(max(0, dot(vec3(0, 0, 1), reflectRay)), 180), 1);

	fragColor = (Color + amb) * mix(text, shit, Smooth) + spec;
}
