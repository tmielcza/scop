#version 400

uniform mat4 viewMatrix, projMatrix;
uniform sampler2D tex;
uniform float tiling;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 Color;
out vec3 Position;
out vec3 Normal;
out vec3 LightVec;
out vec3 rNormal;
flat out vec4 shit;

void main()
{
	vec3 lightPos = vec3(1000, 1000, 1000);
	vec3 rnormal = transpose(inverse(mat3(viewMatrix))) * normal;
	vec4 pos = projMatrix * viewMatrix * vec4(position, 1);
	LightVec = normalize(pos.xyz - lightPos);
	float col = mod((position.x + position.y + position.z) * tiling / 3.0, 0.8) + 0.2;
	col = min(max(0, col), 1);
	float rcol = max(0, dot(rnormal, LightVec));
	gl_Position = pos;
	Position = position;
	Normal = normal;
	rNormal = rnormal;

	Color = vec4(rcol, rcol, rcol, 1);
	shit = vec4(col, col, col, 1);
}
